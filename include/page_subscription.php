<?php 
	include("head.php"); 

	$success;
	if (isset($_GET['confirm']))
		$success = $SUBSCRIPTION->confirm($_GET['confirm']);
	else $success = false;
?>

<body>
		
		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2><a href="#">Conferma iscrizione</a></h2>
						<p><?php echo $success ? 'Operazione riuscita' : 'Operazione non riuscita'; ?></p>
					</header>
					<p>
						<?php echo $success ? 'L\' iscrizione all\' evento è andata a buon fine. La invitiamo a controllare periodicamente la casella di posta elettronica fornita per rimanere informato in caso di aggiornamenti.' : 'Qualcosa è andato storto, la invitiamo a ripetere la procedura e in caso il problema persista inviare una email all\' indirizzo: <strong>cenacoloandreadagrosseto@gmail.com<strong>'; ?>
						<br /><a href="../index.php?page=home">Torna alla Home Page</a>
					</p>
				</article>

			</div>
		</div>

		<?php include("sidebar.php"); ?>


		<?php include("scripts.php"); ?>

</body>