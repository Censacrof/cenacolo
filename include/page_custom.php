<?php include("head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<?php 
					
					$limit = 3;
					$totPosts = $PAGE->getPostCount($CURR_PAGE);
					$pageno = isset($_GET['pageno']) ? (int) $_GET['pageno'] : 0;

					$posts = $PAGE->getPostsOffset($CURR_PAGE, $pageno, $limit); 
					
					foreach ($posts as $post) 
					{
						echo '<article class="box post post-excerptz"><header>
						<h2>'.$post->title.'</h2>
						<p>'.$post->subtitle.'</p></header>
						<p>'.$post->body.'</p>
						<div class = "post-date">Creato: '.sqlToDateTime($post->date).'</div>
						</article>
						<hr />';
					}

					echoPagination($CURR_PAGE, $totPosts, $limit, $pageno);
				?>

			</div>
		</div>

		<?php include("sidebar.php"); ?>


		<?php include("scripts.php"); ?>

</body>