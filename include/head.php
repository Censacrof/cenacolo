<head>
	<title><?php echo $TITLE; ?></title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lte IE 8]><script src="../js/ie/html5shiv.js"></script><![endif]-->
	<link rel="stylesheet" href="../css/main.css" />
	<!--[if lte IE 8]><link rel="stylesheet" href="../css/ie8.css" /><![endif]-->

	<link rel="stylesheet" href="../css/custom.css" />


	<link rel="stylesheet" href="../css/flatpickr.min.css" />
	<script src="js/flatpickr.min.js"></script>

	<script src="ckeditor/ckeditor.js"></script>
	<script src="js/sorttable.js"></script>	

	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>