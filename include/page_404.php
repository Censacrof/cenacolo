<?php include("head.php"); ?>

<body>
		
		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2><a href="#">Error 404:</a></h2>
						<p>Not found.</p>
					</header>
					<p>
						La pagina che stai cercando non esiste. Probabilmente è stata rimossa. <br />
						<a href="../index.php?page=home">Torna alla Home Page</a>
					</p>
				</article>

			</div>
		</div>

		<?php include("sidebar.php"); ?>


		<?php include("scripts.php"); ?>

</body>