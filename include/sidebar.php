<div id="sidebar">

	<!-- Logo -->
		<h1 id="logo"><a href="../index.php?page=home">Cenacolo</a></h1>

		<nav id="nav">
			<ul>
				<?php 
					
					$plist = $PAGE->getPageList();

					foreach ($plist as $page) 
					{
						$lname = strtolower($page->name);
						echo '<li';
						if ($CURR_PAGE == $lname)
							echo ' class="current"';
						echo '><a href="index.php?page='.urlencode($lname).'">'.$page->name.'</a></li>';
					}

				?>
			</ul>
		</nav>

		<ul id="copyright">
			<li><?php echo $COPYRIGHT; ?></a></li>
		</ul>

		<p><a href="index.php?admin"><div class="icon fa-cog" style="padding: 1em;"> Amministrazione</div></a></p>
</div>