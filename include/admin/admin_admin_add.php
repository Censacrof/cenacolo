<?php include("include/head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2>Area di amministrazione</h2>
						<p>Aggiungi relatore</p>
					</header>

					<form method="post" action="actions/action_admin_admin_add.php">

						<?php
							if (isset($_GET['error']))
								echo '<div class="error" id="error">Riempi tutti i campi e assicurati che non esista già lo stesso amministratore prima di inviare</div>';
						?>
						
						<p>Nome</p>
						<input type="text" name="name" maxlength="20" placeholder="massimo 20 caratteri" /><br />

						<p>Cognome</p>
						<input type="text" name="surname" maxlength="20" placeholder="massimo 20 caratteri" /><br />

						<p>Email</p>
						<input type="email" name="email" /><br />

						<button type="submit">Aggiungi amministratore</button>
					</form>

				</article>

			</div>
		</div>

		<?php include("include/admin/admin_sidebar.php"); ?>
		<?php include("include/scripts.php"); ?>

</body>