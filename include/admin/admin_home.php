<?php include("include/head.php"); ?>

<body>	
		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2>Area di amministrazione</h2>
					</header>
					
					<p>Da qui puoi gestire i contenuti del sito e gli utenti.</p>

					<section id="_immagini">
						<header>
							<h2>Aggiungi immagine</h2>
							<p>Carica immagini da poter utilizzare in seguito all' interno di posts</p>
						</header>

						<p>Per questioni di praticità le immagini del sito verranno hostate su un server esterno (per esempio postimage.org).<br />
						Ciò permette di salvare spazio di archiviazione sul proprio server, diminuendone così i costi e gli interventi di manutenzione.</p>
						<p><strong>Istruzioni:</strong><br />
							<ul class="normal">
								<li>Cliccare sul pulsante qui sotto per aprire postimage.org in una nuova scheda del browser.</li>
								<li>Nella scheda di postimage.org cliccare su "Scegliere le immagini".</li>
								<li>Selezionare l' immagine che si vuole caricare dal proprio computer traminte la finestra di selezione file che si è aperta.</li>
								<li>Una volta terminato l' upload dell' immagine copiare e conservare il link nella parte bassa della pagina alla voce "Collegamento Diretto".</li>
								<li>Per inserire l' immagine in un post:</li>
								<ul class="normal">
									<li>Durante l' inserimento di un post cliccare sul pulsante <img style="margin: 0;" src="images/ck_img_icon.png" /> aggiungi immagine situato all' interno dell' editor di testo.</li>
									<li>Incollare il link dell' immagine nel campo "url", inserire a piacimento le altre proprietà dell' immagine e cliccare su ok.</li>
								</ul>
							</ul><br />
							Per avere un maggior controllo sulle immagini si consiglia di registrare un account su postimage.org. Ciò permetterà di gestire le immagini già caricate.
						</p>

						<a class="button" href="https://postimage.org/" target="_blank">Postimage.org</a>
						<hr>

					</section>

					<?php if ($ADMIN->getLevel() > 0) echo '<section id="_admins">
						<header>
							<h2>Amministratori</h2>
							<p>Visualizza informazioni sugli amministratori</p>
						</header>
						
						<a class="button" href="index.php?admin=admin_add">Aggiungi amministratore</a>
						<a class="button" href="index.php?admin=admin_list">Lista amministratori</a>
						<hr>
					</section>'; ?>

					<section id="_post">
						<header>
							<h2>Post</h2>
							<p>Aggiungi, modifica, rimuovi post</p>
						</header>

						<a class="button" href="index.php?admin=post_add">Aggiungi post</a>
						<a class="button" href="index.php?admin=post_list">Lista posts</a>
						<hr>

					</section>

					<section id="_pagine">
						<header>
							<h2>Pagine</h2>
							<p>Aggiungi, modifica, rimuovi pagine</p>
						</header>

						<a class="button" href="index.php?admin=page_add">Aggiungi pagina</a>
						<a class="button" href="index.php?admin=page_list">Lista pagine</a>
						<hr>

					</section>

					</section>

					<section id="_relatori">
						<header>
							<h2>Relatori</h2>
							<p>Aggiungi, modifica, rimuovi relatori</p>
						</header>
						
						<a class="button" href="index.php?admin=supervisor_add">Aggiungi relatore</a>
						<a class="button" href="index.php?admin=supervisor_list">Lista relatore</a>
						<hr>

					</section>

					<section id="_eventi">
						<header>
							<h2>Evento</h2>
							<p>Aggiungi, modifica, rimuovi evento</p>
						</header>
						
						<a class="button" href="index.php?admin=event_add">Aggiungi evento</a>
						<a class="button" href="index.php?admin=event_list">Lista eventi</a>
						<hr>

					</section>

				</article>

			</div>
		</div>

		<?php include("include/admin/admin_sidebar.php"); ?>
		<?php include("include/scripts.php"); ?>

</body>
