
<?php include("include/head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2>Area di amministrazione</h2>
						<p>Aggiungi pagina</p>
					</header>

					<form method="post" action="actions/action_admin_page_add.php">
						
						<?php if (isset($_GET['error'])) echo '<div class="error">Impossibile creare la pagina. Esiste già una pagina con lo stesso nome.</div>'; ?>

						<p>Nome della pagina</p>
						<input type="text" name="name" placeholder="Massimo 20 caratteri" />
						<br />

						<button type="submit">Crea pagina</button>

					</form>

				</article>

			</div>
		</div>

		<?php include("include/admin/admin_sidebar.php"); ?>
		<?php include("include/scripts.php"); ?>

</body>