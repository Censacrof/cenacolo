
<?php 
	if (!isset($_GET['idEvent']))
	{
		header("Location: index.php?admin");
		exit;
	}

	$subs = $SUBSCRIPTION->getSubscribers($_GET['idEvent']);
	$ev = $EVENT->getEvent($_GET['idEvent']);
?>
<head>
	<title>Lista iscrizioni</title>
</head>

<body>
		

		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2><?php echo $ev->title; ?></h2>
						<p>Relatore <?php echo $ev->supervisor; ?>. Data e ora: <?php echo sqlToDateTime($ev->date); ?></p>
						<p>Lista iscrizioni (<?php $par = $SUBSCRIPTION->getParticipants($_GET['idEvent']); echo $par == 0 ? '0' : $par; ?> partecipanti)</p>
					</header>

					<table style="border-collapse: collapse;" border="1px">
						<tr>
							<th>Nome</th>
							<th>Cognome</th>
							<th>Email</th>
							<th>Posti</th>
							<th>Pasto</th>
						</tr>

						<?php

							foreach ($subs as $sub) 
							{
								echo '<tr><td>'.$sub->name.'</td>
								<td>'.$sub->surname.'</td>
								<td>'.$sub->email.'</td>
								<td>'.$sub->sits.'</td>
								<td>'; echo $sub->meal == 1 ? "si" : "no"; echo '</td></tr>';
							}
						?>

					</table>

				</article>

			</div>
		</div>

</body>