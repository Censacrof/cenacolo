<?php

	$mod = false; 
	$modSuper;
	if (isset($_GET['mod']))
	{
		$mod = true;
		$modSuper = $SUPERVISOR->getSupervisor($_GET['mod']);
		if ($modSuper === false)
		{
			header("Location: ../index.php?admin=supervisor_list");
			exit;
		}
	}
?>
	
<?php include("include/head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2>Area di amministrazione</h2>
						<p><?php echo $mod ? 'Modifica relatore' : 'Aggiungi relatore' ?></p>
					</header>

					<form method="post" action="<?php echo $mod ? 'actions/action_admin_supervisor_update.php' : 'actions/action_admin_supervisor_add.php'?>">

						<?php
							if (isset($_GET['error']))
								echo '<div class="error" id="error">Riempi tutti i campi e assicurati che non esista già lo stesso relatore prima di inviare</div>';
						?>
						
						<p>Nome</p>
						<input type="text" name="name" maxlength="20" placeholder="massimo 20 caratteri" 
							<?php if ($mod) echo 'value="'.htmlspecialchars($modSuper->name).'"'; ?>
						 /><br />

						<p>Cognome</p>
						<input type="text" name="surname" maxlength="20" placeholder="massimo 20 caratteri"
							<?php if ($mod) echo 'value="'.htmlspecialchars($modSuper->surname).'"'; ?>
						 /><br />

						<p>Email</p>
						<input type="email" name="email"
							<?php if ($mod) echo 'value="'.htmlspecialchars($modSuper->email).'"'; ?> 
						/><br />
						
						<textarea id="ck" name="body"><?php if ($mod) echo $modSuper->body; ?></textarea><br />
						<button type="submit"><?php echo $mod ? 'Modifica relatore ' : 'Aggiungi relatore'; ?></button>

						<?php if ($mod) echo '<input type="hidden" name="id" value="'.$modSuper->ID.'" />'; ?>

						<script type="text/javascript">
							CKEDITOR.replace('ck');
						</script>
					</form>

				</article>

			</div>
		</div>

		<?php include("include/admin/admin_sidebar.php"); ?>
		<?php include("include/scripts.php"); ?>

</body>