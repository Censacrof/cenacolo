<div id="sidebar" class="fixed">

	<h1 id="logo"><a href="index.php">Cenacolo</a></h1>
	
	<nav id="nav">
		<ul>
			<li><a href="index.php?admin=home#_immagini">Immagini</a></li>
			<?php if ($ADMIN->getLevel() > 0) echo '<li><a href="index.php?admin=home#_admins">Amministratori</a></li>'; ?>
			<li><a href="index.php?admin=home#_post">Post</a></li>
			<li><a href="index.php?admin=home#_pagine">Pagine</a></li>
			<li><a href="index.php?admin=home#_relatori">Relatori</a></li>
			<li><a href="index.php?admin=home#_eventi">Eventi</a></li>
			<li><a href="actions/action_admin_logout.php">Esci</a></li>
		</ul>
	</nav>

	<ul id="copyright">
		<li><?php echo $COPYRIGHT; ?></a></li>
	</ul>


</div>