<?php

	$mod = false; 
	$modEvent;
	if (isset($_GET['mod']))
	{
		$mod = true;
		$modEvent = $EVENT->getEvent($_GET['mod']);
		if ($modEvent === false)
		{
			header("Location: ../index.php?admin=event_list");
			exit;
		}
	}
?>

<?php include("include/head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2>Area di amministrazione</h2>
						<p><?php echo $mod == false ? 'Aggiungi evento' : 'Modifica evento'; ?></p>
					</header>

					<form method="post" action="actions/<? echo $mod ? 'action_admin_event_update.php' : 'action_admin_event_add.php'; ?>">

						<?php
							if (isset($_GET['error']))
								echo '<div class="error" id="error">Riempi tutti i campi obbligatori prima di inviare</div>';
						?>
						
						<p>Titolo</p>
						<input type="text" name="title" maxlength="50" placeholder="massimo 50 caratteri" 
							<?php if ($mod) echo 'value="'.htmlspecialchars($modEvent->title).'"'; ?>
						/><br />
						
						<p>Sottotitolo</p>
						<input type="text" name="subtitle" maxlength="200" placeholder="massimo 200 caratteri" 
							<?php if ($mod) echo 'value="'.htmlspecialchars($modEvent->subtitle).'"'; ?>
						/><br />
						
						<p>Data e ora</p>
						<input class="flatpickr" type="text" name="date" placeholder="Seleziona data e ora" /><br />
						<script type="text/javascript">
							flatpickr(".flatpickr", {
								<?php if ($mod) echo "defaultDate: '".$modEvent->date."',"; ?>
							    enableTime: true,
							    time_24hr: true
							});
						</script>

						<p>Chi è il relatore?</p>
						<select name="idSupervisor">
							<?php
								$supervisors = $SUPERVISOR->getSupervisorList();
								foreach ($supervisors as $supervisor)
								{
									echo '<option '; 
									if ($mod) if ($supervisor->ID == $modEvent->idSupervisor)
										echo 'selected="selected" ';
									echo 'value="'.$supervisor->ID.'">'.$supervisor->name.' '.$supervisor->surname.'</option>';
								}
							?>
						</select><br />


						<textarea id="ck" name="body">
							<?php if ($mod) echo $modEvent->body ?>
						</textarea><br />

						<?php if ($mod) echo '<input type="hidden" name="id" value="'.$modEvent->ID.'" />'; ?>

						<button type="submit"><?php echo $mod == false ? 'Aggiungi evento' : 'Modifica evento'; ?></button>

						<script type="text/javascript">
							CKEDITOR.replace('ck');
						</script>
					</form>

				</article>

			</div>
		</div>

		<?php include("include/admin/admin_sidebar.php"); ?>
		<?php include("include/scripts.php"); ?>

</body>