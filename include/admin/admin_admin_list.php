<?php include("include/head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2>Area di amministrazione</h2>
						<p>Lista amministratori</p>
					</header>

					<table class = "list-table sortable">
						<tr>
							<th>ID</th>
							<th>Nome</th>
							<th>Cognome</th>
							<th>Email</th>
							<th class="sorttable_nosort">Elimina</th>
						</tr>

						<script type="text/javascript">
							function confirmDelete(id, name)
							{
								if (confirm("Sei sicuro di voler eliminare l' amministratore: '" + name +"' ?") == true)
								{
									window.location.replace('actions/action_admin_admin_remove.php?ID=' + id);
								} 

							}
						</script>

						<?php
							$admins = $ADMIN->getList();

							foreach ($admins as $admin) 
							{
								echo '<tr><td>'.$admin->ID.'</td>
								<td>'.$admin->name.'</td>
								<td>'.$admin->surname.'</td>
								<td>'.$admin->email.'</td>
								<td style="text-align: center;">';
								if ((int) $admin->level == 0 ) echo  '<a onClick = "confirmDelete('.$admin->ID.',\''.$admin->name.' '.$admin->surname.'\');" class="button">Elimina</a>';
								else
									echo '<div class="error">Non disponibile</div>';
								echo '</td></tr>';
							}
						?>

					</table>

				</article>

			</div>
		</div>

		<?php include("include/admin/admin_sidebar.php"); ?>
		<?php include("include/scripts.php"); ?>

</body>