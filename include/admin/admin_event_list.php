<?php include("include/head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2>Area di amministrazione</h2>
						<p>Lista eventi</p>
					</header>

					<table class = "list-table sortable">
						<tr>
							<th>ID</th>
							<th>Titolo</th>
							<th>Sottotitolo</th>
							<th>Relatore</th>
							<th class="sorttable_date">Data</th>
							<th class="sorttable_nosort">Modifica</th>
							<th class="sorttable_nosort">Elimina</th>
							<th class="sorttable_nosort">Iscrizioni</th>
						</tr>

						<script type="text/javascript">
							function confirmDelete(id, title)
							{
								if (confirm("Sei sicuro di voler eliminare l' evento: '" + title +"' ? In seguito alla cancellazione non potrà più essere recuperato.") == true)
								{
									window.location.replace('actions/action_admin_event_remove.php?ID=' + id);
								} 

							}
						</script>

						<?php
							$events = $EVENT->getEventList();

							foreach ($events as $event) 
							{
								echo '<tr><td>'.$event->ID.'</td>
								<td>'.$event->title.'</td>
								<td>'.$event->subtitle.'</td>
								<td>'.$event->supervisor.'</td>
								<td sorttable_customkey="'.sqlToSortTable($event->date).'">'.sqlToDateTime($event->date).'</td>
								<td style="text-align: center;">
									<a class="button" href="index.php?admin=event_add&mod='.$event->ID.'">Modifica</a>
								</td>
								<td style="text-align: center;">
									<a onClick = "confirmDelete('.$event->ID.',\''.$event->title.'\');" class="button">Elimina</a>
								</td>
								<td><a target="__blank" href="index.php?admin=subscription_list&idEvent='.$event->ID.'" class="icon fa-users"> '.$SUBSCRIPTION->getParticipants($event->ID).'</a></td></tr>';
							}
						?>

					</table>

				</article>

			</div>
		</div>

		<?php include("include/admin/admin_sidebar.php"); ?>
		<?php include("include/scripts.php"); ?>

</body>