<?php

	$mod = false; 
	$modPost;
	if (isset($_GET['mod']))
	{
		$mod = true;
		$modPost = $POST->getPost($_GET['mod']);
		if ($modPost === false)
		{
			header("Location: ../index.php?admin=post_list");
			exit;
		}
	}
?>

	
<?php include("include/head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2>Area di amministrazione</h2>
						<p><?php echo $mod == false ? 'Aggiungi post' : 'Modifica Post'; ?></p>
					</header>

					<form method="post" action="actions/<?php echo $mod == false ? 'action_admin_post_add.php' : 'action_admin_post_update.php'; ?>">

						<?php
							if (isset($_GET['error']))
								echo '<div class="error" id="error">Riempi tutti i campi obbligatori prima di inviare</div>';
						?>
						

						<p>In quale pagina lo vuoi pubblicare?</p>
						<select name="idPage">
							<?php
								$pages = $PAGE->getSimplePostPageList();
								foreach ($pages as $page)
								{
									echo '<option '; 
									if ($mod) if ($page->ID == $modPost->idPage)
										echo 'selected="selected" ';
									echo 'value="'.$page->ID.'">'.$page->name.'</option>';
								}
							?>
						</select><br />
						
						<p>Titolo</p>
						<input type="text" 
						 <?php if($mod) echo 'value="'.htmlspecialchars($modPost->title).'"'; ?>
						 name="title" maxlength="50" placeholder="massimo 50 caratteri" /><br />
						
						<p>Sottotitolo</p>
						<input type="text"
						 <?php if($mod) echo 'value="'.htmlspecialchars($modPost->subtitle).'"'; ?>
						 name="subtitle" maxlength="200" placeholder="massimo 200 caratteri" /><br /><br />
						
						<textarea id="ck" name="body"><?php if ($mod) echo $modPost->body; ?></textarea><br />
						<button type="submit"><?php echo $mod == false ? 'Aggiungi post' : 'Modifica Post'; ?></button>
						
						<?php if ($mod) echo '<input type="hidden" name="id" value="'.$modPost->ID.'" />'; ?>

						<script type="text/javascript">
							CKEDITOR.replace('ck');
						</script>
					</form>

				</article>

			</div>
		</div>

		<?php include("include/admin/admin_sidebar.php"); ?>
		<?php include("include/scripts.php"); ?>

</body>