
<?php include("include/head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2>Area di amministrazione</h2>
						<p>Login</p>
					</header>
						<form method="post" action="/actions/action_admin_login.php" style="max-width: 15em;">
							<?php if(isset($_GET['error'])) echo '<p class="error">Email o password errata.</p>'; ?>
							<p>Email</p>
							<input type="email" name="email" /><br />
							<p>Password</p>
							<input type="password" name="password" /><br />
							<button type="submit">Login</button>
						</form>
				</article>

			</div>
		</div>

		<?php include("include/sidebar.php"); ?>
		<?php include("include/scripts.php"); ?>

</body>