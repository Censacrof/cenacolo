
<?php include("include/head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2>Area di amministrazione</h2>
						<p>Lista posts</p>
					</header>

					<table class = "list-table sortable">
						<tr>
							<th>ID</th>
							<th>Titolo</th>
							<th>Sottotitolo</th>
							<th class="sorttable_date">Data</th>
							<th>Pagina</th>
							<th class="sorttable_nosort">Modifica</th>
							<th class="sorttable_nosort">Elimina</th>
						</tr>

						<script type="text/javascript">
							function confirmDelete(id, title)
							{
								if (confirm("Sei sicuro di voler eliminare il post: '" + title +"' ? In seguito alla cancellazione non potrà essere recuperato.") == true)
								{
									window.location.replace('actions/action_admin_post_remove.php?ID=' + id);
								} 

							}
						</script>

						<?php
							$posts = $POST->getSimplePosts();

							foreach ($posts as $post) 
							{
								echo '<tr><td>'.$post->ID.'</td>
								<td>'.$post->title.'</td>
								<td>'.$post->subtitle.'</td>
								<td sorttable_customkey="'.sqlToSortTable($post->date).'">'.sqlToDateTime($post->date).'</td><td>'.$post->name.'</td>
								<td style="text-align: center;"><a class="button" href="index.php?admin=post_add&mod='.$post->ID.'">Modifica</a></td>
								<td style="text-align: center;"><a onClick = "confirmDelete('.$post->ID.',\''.$post->title.'\');" class="button">Elimina</a></td></tr>';
							}
						?>

					</table>

				</article>

			</div>
		</div>

		<?php include("include/admin/admin_sidebar.php"); ?>
		<?php include("include/scripts.php"); ?>

</body>