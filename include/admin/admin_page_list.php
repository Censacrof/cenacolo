
<?php include("include/head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2>Area di amministrazione</h2>
						<p>Lista pagine</p>
					</header>

					<table class = "list-table sortable">
						<tr>
							<th>ID</th>
							<th>Nome</th>
							<th>Numero di posts</th>
							<th class="sorttable_nosort">Svuota</th>
							<th class="sorttable_nosort">Elimina</th>
						</tr>

						<script type="text/javascript">
							function confirmDelete(id, name)
							{
								if (confirm("Sei sicuro di voler eliminare la pagina: '" + name +"' ? In seguito alla cancellazione verranno cancellati anche tutti i post correlati e non potranno essere recuperati.") == true)
								{
									window.location.replace('actions/action_admin_page_remove.php?ID=' + id);
								} 

							}

							function confirmEmpty(id, name)
							{
								if (confirm("Sei sicuro di voler eliminare tutti i post della pagina: '" + name +"' ? In seguito alla cancellazione i post non potranno essere recuperati.") == true)
								{
									window.location.replace('actions/action_admin_page_empty.php?ID=' + id);
								} 

							}
						</script>

						<?php
							$pages = $PAGE->getPageList(true);

							foreach ($pages as $page) 
							{
								echo '<tr><td>'.$page->ID.'</td>
								<td>'.$page->name.'</td>
								<td>'.$page->nPosts.'</td>
								<td style="text-align: center;">';

								if ($page->simplePost)
									echo '<a onClick = "confirmEmpty('.$page->ID.',\''.$page->name.'\');" class="button">Svuota</a>';
								else 
									echo '<div class="error">Non disponibile</div>';

								echo '</td><td style="text-align: center;">';

								if (!$page->permanent)
									echo '<a onClick = "confirmDelete('.$page->ID.',\''.$page->name.'\');" class="button">Elimina</a>';
								else 
									echo '<div class="error">Permanente</div>';

								echo '</td></tr>';
							}
						?>

					</table>

				</article>

			</div>
		</div>

		<?php include("include/admin/admin_sidebar.php"); ?>
		<?php include("include/scripts.php"); ?>

</body>