
<?php include("include/head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2>Area di amministrazione</h2>
						<p>Lista realatori</p>
					</header>

					<table class = "list-table sortable">
						<tr>
							<th>ID</th>
							<th>Nome</th>
							<th>Cognome</th>
							<th>Email</th>
							<th class="sorttable_nosort">Modifica</th>
							<th class="sorttable_nosort">Elimina</th>
						</tr>

						<script type="text/javascript">
							function confirmDelete(id, name)
							{
								if (confirm("Sei sicuro di voler eliminare il relatore: '" + name + "' ? In seguito alla cancellazione verrà cancellata anche la biografia e gli eventi correlati e non potranno essere recuperati.") == true)
								{
									window.location.replace('actions/action_admin_supervisor_remove.php?ID=' + id);
								} 

							}
						</script>

						<?php
							$supervisors = $SUPERVISOR->getSupervisorList(true);

							foreach ($supervisors as $super) 
							{
								echo '<tr><td>'.$super->ID.'</td>
								<td>'.$super->name.'</td>
								<td>'.$super->surname.'</td>
								<td>'.$super->email.'</td>

								<td style="text-align: center;">
									<a class="button" href="index.php?admin=supervisor_add&mod='.$super->ID.'">Modifica</a>
								</td>

								<td style="text-align: center;">
									<a onClick = "confirmDelete('.$super->ID.',\''.$super->name." ".$super->surname.'\');" class="button">Elimina</a>
								</td></tr>';
							}
						?>

					</table>

				</article>

			</div>
		</div>

		<?php include("include/admin/admin_sidebar.php"); ?>
		<?php include("include/scripts.php"); ?>

</body>