<?php include("head.php"); ?>

<body>
		
		<div id="content">
			<div class="inner">

				<article class="box post post-excerpt">
					<header>
						<h2><a href="#">Sito temporaneamente inagibile</a></h2>
						<p>DB connection not found</p>
					</header>
					<p>
						Il sito è momentaneamente inutilizzabile, probabilmente per motivi di manutenzione. <br />
						La invitiamo a riprovare più tardi.
					</p>
				</article>

			</div>
		</div>

		<?php include("scripts.php"); ?>

</body>