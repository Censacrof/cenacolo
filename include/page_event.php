<?php include("head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				

				<?php
					if (isset($_GET['id'])) 
					{//mostro evento in particolare
						$event = $EVENT->getEvent($_GET['id']);
						if ($event === false)
						{
							header("Location: ../index.php?page=404");
							exit;
						}

						echo '<article class="box post post-excerpt">
							<header>
								<h2>'.$event->title.'</h2>
								<p>'.$event->subtitle.'</p>
								<h1>Con <a href="index.php?page=relatori&id='.$event->idSupervisor.'">'.$event->supervisor.'</a></h1>
							</header>
							<div class="info">
								'.sqlToSpanDate($event->date);
								$participants = $SUBSCRIPTION->getParticipants($event->ID);
								if ($participants > 0) echo '<ul class="stats"><li><a class="icon fa-users">'.$participants.'</a></li></ul>';
						echo '</div>
							<div style="text-align: right;">
								<a class="button" href="#__subscription"> Iscriviti all\' evento</a>
							</div>
							<p>'.$event->body.'</p>
						</article><hr>';

						if (new DateTime() < new DateTime($event->date))
						{

							echo   '<form id="__subscription" style="max-width: 20em;" method="post" action="actions/action_subscribe.php">
									    <h1 style="font-size: 150%;">Iscriviti all\' evento</h1>';

									if (isset($_GET['error']))
										echo '<p class="error">Non è stato possibile creare una richiesta di iscrizione. La invitiamo a riprovare assicurandosi di aver riempito correttamente tutti i campi</p>';
									else if (isset($_GET['success']))
										echo '<p style="color: green;">Richiesta di iscrizione creata con successo. Per completare l\'operazione cliccare sul link di conferma nell\' email inviata all\' indirizzo fornito. (controllare anche lo spam)</p>';

							echo	   '<span>Nome</span>
										<input style="margin: 0.5em;" placeholder="Mario" type="text" name="name" />
										<span>Cognome</span>
										<input style="margin: 0.5em;" placeholder="Rossi" type="text" name="surname" />
										<span>Email</span>
										<input style="margin: 0.5em;" placeholder="esempio@esempio.com" type="email" name="email" />
										<span>Numero di posti prenotati?</span>
										<select style="margin: 0.5em;" name="sits">
											<option selected="selected" value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="7">7</option>
											<option value="8">8</option>
											<option value="9">9</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
										</select>
										<span>Si desidra partecipare al pasto?</span>
										<select style="margin: 0.5em;" name="meal">
											<option selected="selected" value="0">no</option>
											<option value="1">si</option>
										</select>
										<input type="hidden" name="idEvent" value="'.$event->ID.'" />
										<span>Completare il captcha qua sotto</span>
										<div class="g-recaptcha" data-sitekey="6LemeAwUAAAAANPVXijMj5LHGxDTnzGDmF2w9kIi"></div>
										<div class="error">Attenzione</div>
										<p style="font-size: 80%;">I dati che vi sono richiesti servono <strong>esclusivamente</strong> per permetterci di generare una lista di iscrizioni per gli eventi e per informarvi riguardo cambiamenti o la presenza di nuovi eventi. <strong>Questi dati non verranno utilizzati in altro modo</strong>. Cliccando sul pulsante qua sotto si accettano queste condizioni.</p>
										<button style="margin: 0.5em;" type="submit">Iscriviti!</button>
									</form>';
						}

					}
						else
					{//mostro lista eventi
						echo '<article class="box post post-excerpt"><header><h2>Eventi</h2><p>Lista eventi</p></header>';
					
						$limit = 5;
						$totEvents = $EVENT->getCount();
						$pageno = isset($_GET['pageno']) ? (int) $_GET['pageno'] : 0;
						$idSupervisor = isset($_GET['idSupervisor']) ? $_GET['idSupervisor'] : NULL;
						$events = $EVENT->getEventListOffset($idSupervisor, $pageno, $limit, true, true);

						if (count($events) == 0)
						{
							echo '<p>Non ci sono eventi nel database</p>';
							/*header("Location: ../index.php?page=404");
							exit;*/
						}


						foreach ($events as $event)
						{
							echo '<article class="box post post-excerpt">
									<header>
										<h2><a href="index.php?page=eventi&id='.$event->ID.'">'.$event->title.'</a></h2>
										<p>'.$event->subtitle.'</p>
										<h1>Con <a href="index.php?page=relatori&id='.$event->idSupervisor.'">'.$event->supervisor.'</a></h1>
									</header>
									<div class="info">
										'.sqlToSpanDate($event->date);
										$participants = $SUBSCRIPTION->getParticipants($event->ID);
										if ($participants > 0) echo '<ul class="stats"><li><a class="icon fa-users">'.$participants.'</a></li></ul>';
							echo	'</div>
									<p>'.$event->body.'<br /><a href="index.php?page=eventi&id='.$event->ID.'">Vedi evento</a></p>
								</article><hr>';
						}

						echoPagination("eventi", $totEvents, $limit, $pageno);
	
						echo '</article>';
					}
				?>


			</div>
		</div>

		<?php include("sidebar.php"); ?>


		<?php include("scripts.php"); ?>

</body>
