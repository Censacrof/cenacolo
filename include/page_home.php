<?php include("head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<?php include("post_home.php"); ?>

				<article class="box post post-excerpt">
					<header>
						<h1 style="font-size: 200%;">Eventi in programma</h1>
						<?php 
							$events = $EVENT->getUpcomingEvents(true);	
							if (count($events) == 0)
								echo '<p>Non ci sono eventi in programma</p><a href="index.php?page=eventi">Vedi la lista degli eventi</a>';
						?>
					</header>

					<?php 

						foreach ($events as $event)
						{
							echo '<article class="box post post-excerpt">
									<header>
										<h2><a href="index.php?page=eventi&id='.$event->ID.'">'.$event->title.'</a></h2>
										<p>'.$event->subtitle.'</p>
										<h1>Con <a href="index.php?page=relatori&id='.$event->idSupervisor.'">'.$event->supervisor.'</a></h1>
									</header>
									<div class="info">
										'.sqlToSpanDate($event->date);
										$participants = $SUBSCRIPTION->getParticipants($event->ID);
										if ($participants > 0) echo '<ul class="stats"><li><a class="icon fa-users">'.$participants.'</a></li></ul>';
							echo	'</div>
									<p>'.$event->body.'<br /><a href="index.php?page=eventi&id='.$event->ID.'">Vedi evento</a></p>
								</article><hr>';
						}
					?>


					
				</article>



				<article class="box post post-excerpt">
					<header>
						<?php
							$limit = 3;
							$totPosts = $PAGE->getPostCount($CURR_PAGE);
							$pageno = isset($_GET['pageno']) ? (int) $_GET['pageno'] : 0;

							$posts = $PAGE->getPostsOffset($CURR_PAGE, $pageno, $limit); 

							if ($totPosts > 0) echo '<h1 style="font-size: 200%;">Posts</h1>';
						?>
						
					</header>

					<?php

						foreach ($posts as $post) 
						{
							echo '<article class="box post post-excerpt"><header>
							<h2>'.$post->title.'</h2>
							<p>'.$post->subtitle.'</p></header>
							<p>'.$post->body.'</p>
							<div class = "post-date">Creato: '.sqlToDateTime($post->date).'</div>
							</article>
							<hr />';
						}

						echoPagination($CURR_PAGE, $totPosts, $limit, $pageno);
					?>
					
				</article>



				

			</div>
		</div>

		<?php include("sidebar.php"); ?>


		<?php include("scripts.php"); ?>

</body>
