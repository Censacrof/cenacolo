<?php include("head.php"); ?>

<body>
		

		<div id="content">
			<div class="inner">

				<?php
					if (isset($_GET['id'])) 
					{//mostro relatore in particolare
						$supervisor = $SUPERVISOR->getSupervisor($_GET['id']);
						if ($supervisor === false)
						{
							header("Location: ../index.php?page=404");
							exit;
						}

						echo '<article class="box post post-excerpt">
							<header>
								<h2>'.$supervisor->name.' '.$supervisor->surname.'</h2>
								<p>Note sul relatore</p>';
								if ($supervisor->nEvents > 0) echo '<h1><a href="index.php?page=eventi&idSupervisor='.$supervisor->ID.'">Ha tenuto '.$supervisor->nEvents.' eventi</a></h1>';
						echo '</header>
							<p>'.$supervisor->body.'</p>
						</article><hr>';

					}
						else
					{//mostro lista relatori
						echo '<article class="box post post-excerpt"><header><h2>Relatori</h2><p>Lista relatori</p></header>';
					
						$limit = 5;
						$totSupervisors = $SUPERVISOR->getCount();
						$pageno = isset($_GET['pageno']) ? (int) $_GET['pageno'] : 0;
						$supervisors = $SUPERVISOR->getSupervisorListOffset($pageno, $limit, true, true);
	
						if (count($supervisors) == 0)
						{
							echo '<p>Non ci sono relatori nel database</p>';
							/*header("Location: ../index.php?page=404");
							exit;*/
						}

						foreach ($supervisors as $supervisor)
						{
							echo '<article class="box post post-excerpt">
									<header>
										<h2><a href="index.php?page=relatori&id='.$supervisor->ID.'">'.$supervisor->name.' '.$supervisor->surname.'</a></h2>
										<p>Note sul relatore</p>';
							if ($supervisor->nEvents > 0) echo '<h1><a href="index.php?page=eventi&idSupervisor='.$supervisor->ID.'">Ha tenuto '.$supervisor->nEvents.' eventi</a></h1>';
							echo	'</header>
									<p>'.$supervisor->body.'<br /><a href="index.php?page=relatori&id='.$supervisor->ID.'">Vedi relatore</a></p>
								</article><hr>';
						}

						echoPagination("relatori", $totSupervisors, $limit, $pageno);
	
						echo '</article>';
					}
				?>


			</div>
		</div>

		<?php include("sidebar.php"); ?>


		<?php include("scripts.php"); ?>

</body>
