<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");
	require_once("../php/page_class.php");

	if ($ADMIN->isLogged() && isset($_GET['ID']))
	{
		$PAGE->emptyPage($_GET['ID']);
		header("Location: ../index.php?admin=page_list");
	} else 	header("Location: ../index.php?admin=login&error");
?>