<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");
	require_once("../php/event_class.php");

	if ($ADMIN->isLogged() && isset($_POST['id']) && isset($_POST['title']) && isset($_POST['subtitle']) && isset($_POST['date']) && isset($_POST['idSupervisor']) && isset($_POST['body']))
	{
		if ($EVENT->update($_POST['id'], $_POST['title'], $_POST['subtitle'], $_POST['date'], $_POST['idSupervisor'], $_POST['body']))
			header("Location: ../index.php?page=eventi&id=".$_POST['id']);
		else
			header("Location: ../index.php?admin=event_add&error&mod=".$_POST['id']);

	} else 	header("Location: ../index.php?admin=login&error");
?>