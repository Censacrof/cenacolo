<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");
	require_once("../php/page_class.php");

	if ($ADMIN->isLogged() && isset($_POST['name']))
	{
		if ($PAGE->add($_POST['name']))
			header("Location: ../index.php?page=".urlencode($_POST['name']));
		else
			header("Location: ../index.php?admin=page_add&error");

	} else 	header("Location: ../index.php?admin=login&error");
?>