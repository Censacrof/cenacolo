<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");
	require_once("../php/supervisor_class.php");

	if ($ADMIN->isLogged() && isset($_POST['name']) && isset($_POST['surname']) && isset($_POST['email']) && isset($_POST['body']))
	{
		$res = $SUPERVISOR->add($_POST['name'], $_POST['surname'], $_POST['email'], $_POST['body']);
			
		if ($res === false)
			header("Location: ../index.php?admin=supervisor_add&error");
		else
			header("Location: ../index.php?page=relatori&id=".$res);			

	} else 	header("Location: ../index.php?admin=login&error");
	
?>