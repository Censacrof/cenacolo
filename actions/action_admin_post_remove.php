<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");
	require_once("../php/post_class.php");

	if ($ADMIN->isLogged() && isset($_GET['ID']))
	{
		$POST->remove($_GET['ID']);
		header("Location: ../index.php?admin=post_list");
	} else 	header("Location: ../index.php?admin=login&error");
	
?>