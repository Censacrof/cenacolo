<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");
	require_once("../php/event_class.php");

	if ($ADMIN->isLogged() && isset($_GET['ID']))
	{
		$EVENT->remove($_GET['ID']);
		header("Location: ../index.php?admin=event_list");

	} else 	header("Location: ../index.php?admin=login&error");
?>