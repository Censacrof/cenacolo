<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");
	require_once("../php/post_class.php");

	if ($ADMIN->isLogged() && isset($_POST['id']) && isset($_POST['title']) && isset($_POST['subtitle']) && isset($_POST['body']) && isset($_POST['idPage']))
	{
		if ($POST->update($_POST['id'], $_POST['title'], $_POST['subtitle'], $_POST['body'], $_POST['idPage']))
			header("Location: ../index.php?page=".strtolower($PAGE->getNameByID($_POST['idPage'])));
		else
			header("Location: ../index.php?admin=post_add&mod=".$_POST['id']."&error#error");

	} else 	header("Location: ../index.php?admin=login&error");
	
?>