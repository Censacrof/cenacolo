<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");
	require_once("../php/event_class.php");

	if ($ADMIN->isLogged() && isset($_POST['title']) && isset($_POST['subtitle']) && isset($_POST['date']) && isset($_POST['idSupervisor']) && isset($_POST['body']))
	{
		if ($EVENT->add($_POST['title'], $_POST['subtitle'], $_POST['date'], $_POST['idSupervisor'], $_POST['body']))
			header("Location: ../index.php?page=eventi");
		else
			header("Location: ../index.php?admin=event_add&error");

	} else 	header("Location: ../index.php?admin=login&error");
?>