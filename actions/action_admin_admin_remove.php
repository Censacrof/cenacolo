<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");
	require_once("../php/event_class.php");

	if ($ADMIN->isLogged() && $ADMIN->getLevel() > 0 && isset($_GET['ID']))
	{
		$ADMIN->remove($_GET['ID']);
		header("Location: ../index.php?admin=admin_list");

	} else 	header("Location: ../index.php?admin=login&error");
?>