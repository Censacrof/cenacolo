<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");
	require_once("../php/post_class.php");

	if ($ADMIN->isLogged() && isset($_POST['title']) && isset($_POST['subtitle']) && isset($_POST['body']) && isset($_POST['idPage']))
	{
		if (!$PAGE->isSimplePost($_POST['idPage']))
			header("Location: ../index.php?admin=post_add&error#error");
		else if ($POST->add($_POST['title'], $_POST['subtitle'], $_POST['body'], $_POST['idPage']))
			header("Location: ../index.php?page=".strtolower($PAGE->getNameByID($_POST['idPage'])));
		else
			header("Location: ../index.php?admin=post_add&error#error");

	} else 	header("Location: ../index.php?admin=login&error");
	
?>