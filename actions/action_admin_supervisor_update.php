<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");
	require_once("../php/supervisor_class.php");

	if ($ADMIN->isLogged() && isset($_POST['id']) && isset($_POST['name']) && isset($_POST['surname']) &&  isset($_POST['email']) && isset($_POST['body']))
	{
		if ($SUPERVISOR->update($_POST['id'], $_POST['name'], $_POST['surname'], $_POST['email'], $_POST['body']))
			header("Location: ../index.php?page=relatori&id=".$_POST['id']);
		else
			header("Location: ../index.php?admin=supervisor_add&mod=".$_POST['id']."&error#error");

	} else 	header("Location: ../index.php?admin=login&error");
	
?>