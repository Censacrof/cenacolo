<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");
	require_once("../php/supervisor_class.php");

	if ($ADMIN->isLogged() && isset($_GET['ID']))
	{
		$SUPERVISOR->remove($_GET['ID']);
		header("Location: ../index.php?admin=supervisor_list");
	} else 	header("Location: ../index.php?admin=login&error");
?>