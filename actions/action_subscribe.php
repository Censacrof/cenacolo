<?php
	require_once("../php/config.php");
	$ROOTPATH = "../";
	require_once("../php/subscription_class.php");

	print_r($_POST);

	if (isset($_POST['name']) && isset($_POST['surname']) && isset($_POST['email']) && isset($_POST['sits']) && isset($_POST['idEvent']) && isset($_POST['meal']) && isset($_POST['g-recaptcha-response']))
	{
		if (!testCaptcha())
		{
			header("Location: ../index.php?page=eventi&id=".$_POST['idEvent']."&error#__subscription");
			exit;
		}

		if ($SUBSCRIPTION->subscribe($_POST['name'], $_POST['surname'], $_POST['email'], $_POST['sits'], $_POST['meal'], $_POST['idEvent']))
			header("Location: ../index.php?page=eventi&id=".$_POST['idEvent']."&success#__subscription");
		else
			header("Location: ../index.php?page=eventi&id=".$_POST['idEvent']."&error#__subscription");
	} else
	{
		$_POST['idEvent'] = isset($_POST['idEvent']) ? $_POST['idEvent'] : -1;
		header("Location: ../index.php?page=eventi&id=".$_POST['idEvent']."&error#__subscription");
	}
?>