<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");

	if (isset($_POST['email']) && isset($_POST['password']) && $ADMIN->login($_POST['email'], $_POST['password']))
		header("Location: ../index.php?admin=home");
	else 
		header("Location: ../index.php?admin=login&error");

?>