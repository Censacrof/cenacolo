<?php
	$ROOTPATH = "../";
	require_once("../php/admin_class.php");
	require_once("../php/event_class.php");

	if ($ADMIN->isLogged() && $ADMIN->getLevel() > 0 && isset($_POST['name']) && isset($_POST['surname']) && isset($_POST['email']))
	{
		if ($ADMIN->add($_POST['name'], $_POST['surname'], $_POST['email']))
			header("Location: ../index.php?admin");
		else
			header("Location: ../index.php?admin=admin_add&error");

	} else 	header("Location: ../index.php?admin=login&error");
?>