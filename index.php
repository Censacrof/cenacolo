<html>

<?php 
	//config file
	include("php/config.php");

	//classes
	require_once("php/database_class.php");
	require_once("php/user_class.php");
	require_once("php/admin_class.php");
	require_once("php/page_class.php");
	require_once("php/post_class.php");
	require_once("php/supervisor_class.php");
	require_once("php/event_class.php");
	require_once("php/subscription_class.php");
	require_once("php/mail_class.php");

	/*----------------- page selector ---------------*/

	$CURR_PAGE;
	if (!$DB->isConnected())
	{
		$CURR_PAGE = "dberror";
		$TITLE .= "Sito inagibile";
		include("include/page_db_error.php");
	}
	else if (!isset($_GET['admin']))
	{
		if (isset($_GET['page']))
		{
			$CURR_PAGE = strtolower(urldecode($_GET['page']));
		} else $CURR_PAGE = 'home';

		switch ($CURR_PAGE) 
		{
			case 'home':
					$TITLE .= "Home Page";
					include("include/page_home.php");
				break;

			case 'eventi':
					$TITLE .= "Eventi";
					include("include/page_event.php");
				break;

			case 'relatori':
					$TITLE .= "Relatori";
					include("include/page_supervisor.php");
				break;

			case 'iscrizione':
					$TITLE .= "Iscrizione";
					include("include/page_subscription.php");
				break;
			
			default:
					if ($PAGE->pageExists($CURR_PAGE))
					{
						$TITLE .= $CURR_PAGE;
						include("include/page_custom.php");
					} else
					{
						$CURR_PAGE = "404";
						$TITLE .= "404 not found";
						include("include/page_404.php");
					}
				break;
		}
	} else
	{
		//admin section
		$CURR_PAGE = $_GET['admin'] != NULL ? "admin_".strtolower($_GET['admin']) : "admin_home";

		if (!$ADMIN->isLogged())
			$CURR_PAGE = "admin_login";

		switch ($CURR_PAGE) 
		{
			case "admin_home":
					$TITLE .= "Amministrazione";
					include("include/admin/admin_home.php");
				break;
			
			case "admin_login":
					$TITLE .= "Login";
					include("include/admin/admin_login.php");
				break;

			case "admin_post_add":
					$TITLE .= isset($_GET['mod']) ? "Modifica post" : "Aggiungi post";
					include("include/admin/admin_post_add.php");
				break;

			case "admin_post_list":
					$TITLE .= "Lista posts";
					include("include/admin/admin_post_list.php");
				break;

			case "admin_page_add":
					$TITLE .= "Aggiungi pagina";
					include("include/admin/admin_page_add.php");
				break;

			case "admin_page_list":
					$TITLE .= "Lista pagine";
					include("include/admin/admin_page_list.php");
				break;

			case "admin_supervisor_add":
					$TITLE .= "Aggiungi relatore";
					include("include/admin/admin_supervisor_add.php");
				break;

			case "admin_supervisor_list":
					$TITLE .= "Lista relatori";
					include("include/admin/admin_supervisor_list.php");
				break;

			case "admin_event_add":
					$TITLE .= "Aggiungi evento";
					include("include/admin/admin_event_add.php");
				break;

			case "admin_event_list":
					$TITLE .= "Lista eventi";
					include("include/admin/admin_event_list.php");
				break;

			case "admin_subscription_list":
					$TITLE .= "Lista iscrizioni";
					include("include/admin/admin_subscription_list.php");
				break;

			case "admin_admin_add":
					$TITLE .= "Aggiungi amministratore";
					if ($ADMIN->getLevel() > 0)
						include("include/admin/admin_admin_add.php");
					else 
						include("include/page_404.php");
				break;

			case "admin_admin_list":
					$TITLE .= "Lista amministratori";
					if ($ADMIN->getLevel() > 0)
						include("include/admin/admin_admin_list.php");
					else 
						include("include/page_404.php");
				break;

			default:
					$CURR_PAGE = "404";
					$TITLE .= "404 not found";
					include("include/page_404.php");
				break;
		}
	}
	
?>

</html>