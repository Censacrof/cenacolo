<?php

	require_once($ROOTPATH."php/database_class.php");
	require_once($ROOTPATH."php/mail_class.php");
	
	class Admin
	{

		private $_DB;
		private $_MAIL;

		function __construct($DB, $MAIL)
		{
			$this->_DB = $DB;
			$this->_MAIL = $MAIL;
		}

		public function login($email, $password)
		{
			$res = $this->_DB->query("select ID, password, salt from admin where email = ? limit 1", array($email));

			$phash = hash("sha256", $password.$res[0]->salt);

			if (count($res) > 0 && $phash == $res[0]->password)
			{//logged
				$s_token = md5(uniqid(rand(), true));
				$s_ip = $_SERVER['REMOTE_ADDR'];

				$this->_DB->query("update admin set s_token = ?, s_ip = ? where ID = ?", array($s_token, $s_ip, $res[0]->ID));

				setcookie("s_token", $s_token, 0, "/");

				return true;
			} else return false;
		}

		public function logout()
		{
			if (isset($_COOKIE['s_token']))
			{
				$this->_DB->query("update admin set s_token = NULL, s_ip = NULL where s_token = ?", array($_COOKIE['s_token']));
				unset($_COOKIE['s_token']);
				setcookie("s_token", "", time()-3600, "/");
			}
			
		}

		public function isLogged()
		{
			if (!isset($_COOKIE['s_token']))
				return false;

			$res = $this->_DB->query("select ID from admin where s_token = ? && s_ip = ? limit 1", array($_COOKIE['s_token'], $_SERVER['REMOTE_ADDR']));

			if (count($res) == 0)
				return false;
			return true;
		}

		public function getLevel()
		{
			$res = $this->_DB->query("select level from admin where s_token = ?", array($_COOKIE['s_token']));
			return (int) $res[0]->level;
		}

		public function add($name, $surname, $email)
		{
			//genero password
			$flat_pass = $this->randomString(10);

			//genero salt
			$salt = $this->randomString(64);

			//genero hash
			$phash = hash("sha256", $flat_pass.$salt);

			//inserisco dati nel db
			$res = $this->_DB->query("insert into admin(name, surname, email, password, salt) values(?, ?, ?, ?, ?)", array($name, $surname, $email, $phash, $salt));

			//in caso di successo invio le credenziali all' email fornita
			if ($res === false)
				return false;
			else
			{
				$this->_MAIL->sendAdminCredentials($email, $flat_pass);
				return true;
			}
		}

		public function remove($id)
		{
			$this->_DB->query("delete from admin where ID = ?", array($id));
		}

		public function getList()
		{
			return $this->_DB->query("select ID, name, surname, email, level from admin");
		}

		private function randomString($length)
		{   
		    $charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

		    $key = "";
		    for($i=0; $i<$length; $i++) 
		        $key .= $charset[(mt_rand(0,(strlen($charset)-1)))]; 

		    return $key;
		}

	} $ADMIN = new Admin($DB, $MAIL);

?>