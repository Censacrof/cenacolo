<?php

	require_once($ROOTPATH."php/database_class.php");

	class Mail
	{
		private $_HEADERS;
		private $_DB;

		function __construct($DB, $from)
		{
			$headers = "From: " .$from. "\r\n";
			$headers .= "Reply-To: ".$from. "\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

			$this->_DB = $DB;
			$this->_HEADERS = $headers;
		}

		public function sendSubscriptionConfirmation($to, $t_confirm)
		{
			$message = '<html><body><p>Per confermare l\' iscrizione all\' evento, e quindi renderla valida, andare al seguente indirizzo:</p>
			<a href="www.cenacoloandreadagrosseto.it/index.php?page=iscrizione&confirm='.$t_confirm.'">www.cenacoloandreadagrosseto.it/index.php?page=iscrizione&confirm='.$t_confirm.'</a><p>In caso di errore, per modificare l\' iscrizione basta ripetere la procedura inserendo i dati corretti e cliccare poi il link fornito nella nuova email di conferma</p></body></html>';

			$this->send($to, "Conferma iscrizione evento", $message);
		}

		public function sendAdminCredentials($to, $password)
		{
			$message = '<html><body><p>La registrazione come amministratore è andata a buon fine.</p><p>Le credenziali di accesso sono:<br/>email-> '.$to.'<br />password-> '.$password.'</p></body></html>';

			$this->send($to, "Crendeziali di amministratore", $message);
		}

		public function sendEventCreation($id, $title, $subtitle, $date)
		{
			$message = '<html><body>Siamo lieti di informarla che un nuovo evento è stato aggiunto nel database:<h2>'.$title.'</h2><h3>'.$subtitle.'</h3><p>L\' evento si terrà in data: '.$this->sqlToDateTime($date).'<br />Per ricevere maggiori informazioni o iscriversi <a href="cenacoloandreadagrosseto.it/index.php?page=eventi&id='.$id.'">clicca qui</a></p></body></html>';

			$res = $this->_DB->query("select email from user");

			foreach ($res as $user)
			{
				$this->send($user->email, "Nuovo evento disponibile", $message);
			}
		}

		public function sendEventUpdate($id)
		{
			$message = '<html><body>Distinto utente,<br /> un evento a cui lei risulta iscritto è stato modificato.<p>Per ricevere maggiori informazioni <a href="cenacoloandreadagrosseto.it/index.php?page=eventi&id='.$id.'">clicca qui</a></p></body></html>';

			$res = $this->_DB->query("select u.email from user u join subscription s on s.idUser = u.ID where s.idEvent = ?", Array($id));

			foreach ($res as $user)
			{
				$this->send($user->email, "Evento modificato", $message);
			}
		}

		public function sendEventRemove($id)
		{
			$res = $this->_DB->query("select p.title, p.subtitle, e.date from event e join post p on e.idPost = p.ID where e.ID = ?", Array($id));	
			$title = $res[0]->title;
			$subtitle = $res[0]->subtitle;
			$date = $res[0]->date;

			$message = '<html><body>Distinto utente,<br /> ci dispiace informarla che un evento a cui lei risulta iscritto('.$title.': '.$subtitle.') è stato annullato.<p>Ci scusiamo per il disagio</p></body></html>';

			$res = $this->_DB->query("select u.email from user u join subscription s on s.idUser = u.ID where s.idEvent = ?", Array($id));

			foreach ($res as $user)
			{
				$this->send($user->email, "Evento annullato", $message);
			}
		}

		public function send($to, $subject, $message)
		{
			mail($to, $subject, $message, $this->_HEADERS);
		}

		private function sqlToDateTime($date)
		{
			$d = new DateTime($date);
			return $d->format("d/m/Y H:i");
		}

	} $MAIL = new Mail($DB, "cenacoloandreadagrosseto@gmail.com");

?>