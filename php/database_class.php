<?php 

	class Database
	{
		public $_pdo;
		private $_isConnected;


		function __construct($db, $username, $password)
		{
			try {
				$this->_pdo = new PDO("mysql:host=localhost;dbname=".$db, $username, $password);
				$this->_isConnected = true;
			} 
			catch (PDOException $e) {
				$this->_isConnected = false;
			}
		}

		public function query($query, $param = Array())
		{
			if (!$this->_isConnected)
				return Array();

			$_st = $this->_pdo->prepare($query);

			if (!$_st->execute($param))
				return false;

			$rows = Array();
			while ($row = $_st->fetch(PDO::FETCH_ASSOC))
			{
				array_push($rows, (object) $row);
			}


			return $rows;
		}


		public function lastInsertId($name = NULL)
		{
			return $this->_pdo->lastInsertId($name);
		}

		public function isConnected() { return $this->_isConnected; }
	}
	$DB = new Database("my_cenacoloandreadagrosseto", "root", "");

?>