<?php

	require_once($ROOTPATH."php/database_class.php");
	require_once($ROOTPATH."php/page_class.php");
	
	class Post
	{
		private $_DB;
		private $_PAGE;

		function __construct($DB, $PAGE)
		{
			$this->_DB = $DB;
			$this->_PAGE = $PAGE;
		}

		public function add($title, $subtitle, $body, $idPage)
		{
			if ($title == NULL || $subtitle == NULL || $body == NULL || $idPage == NULL)
				return false;

			if (!$this->_PAGE->pageExistsID($idPage))
				return false;

			$this->_DB->query("insert into post(title, subtitle, body, idPage) values(?, ?, ?, ?)", Array($title, $subtitle, $body, $idPage));
			return true;
		}

		public function remove($id)
		{
			$this->_DB->query("delete from post where ID = ?", Array($id));
		}

		public function getPost($id)
		{
			$res = $this->_DB->query("select * from post where ID = ?", Array($id));

			if (count($res) > 0)
				return $res[0];
			else return false;
		}

		public function update($id, $title, $subtitle, $body, $idPage)
		{
			if ($id == NULL || $title == NULL || $subtitle == NULL || $body == NULL || $idPage == NULL)
				return false;

			if (!$this->_PAGE->pageExistsID($idPage))
				return false;

			$this->_DB->query("update post set title = ?, subtitle = ?, body =  ?, idPage = ? where ID = ?", Array($title, $subtitle, $body, $idPage, $id));
			return true;
		}

		public function getSimplePosts()
		{
			return $this->_DB->query("select po.ID, po.title, po.subtitle, po.date, pa.name from post po join page pa on po.idPage = pa.ID where pa.simplePost = true order by pa.name, po.date desc");
		}

	} $POST = new Post($DB, $PAGE);

?>