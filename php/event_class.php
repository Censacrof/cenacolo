<?php
	
	require_once($ROOTPATH."php/database_class.php");
	require_once($ROOTPATH."php/supervisor_class.php");
	require_once($ROOTPATH."php/mail_class.php");

	class Event
	{
		private $_DB;
		private $_SUPERVISOR;
		private $_POST;
		private $_MAIL;

		function __construct($DB, $SUPERVISOR, $POST, $MAIL)
		{
			$this->_DB = $DB;
			$this->_SUPERVISOR = $SUPERVISOR;
			$this->_POST = $POST;
			$this->_MAIL = $MAIL;
		}

		public function add($title, $subtitle, $date, $idSupervisor, $body)
		{
			if ($title == NULL || $subtitle == NULL || $date == NULL || $idSupervisor == NULL || $body == NULL)
				return false;

			$this->_DB->_pdo->beginTransaction();

			if (!$this->_POST->add($title, $subtitle, $body, 2))
			{
				$this->_DB->_pdo->rollBack();

				return false;
			}

			if ($this->_DB->query("insert into event(date, idSupervisor, idPost) values(?, ?, ?)", Array($date, $idSupervisor, $this->_DB->lastInsertId())) === false)
			{
				$this->_DB->_pdo->rollBack();

				return false;
			}

			$this->_MAIL->sendEventCreation($this->_DB->lastInsertId(), $title, $subtitle, $date);
			$this->_DB->_pdo->commit();


			return true;
		}

		public function getEventList($includeBody = false, $stripAndChop = false, $chars = 400)
		{
			$res;
			if ($includeBody)
				$res = $this->_DB->query("select e.ID, p.title, p.subtitle, p.body, CONCAT(s.name, ' ', s.surname) as supervisor, e.date, e.idSupervisor from supervisor s join event e join post p on e.idSupervisor = s.ID && e.idPost = p.ID order by e.date desc");
			else
				$res = $this->_DB->query("select e.ID, p.title, p.subtitle, CONCAT(s.name, ' ', s.surname) as supervisor, e.date, e.idSupervisor from supervisor s join event e join post p on e.idSupervisor = s.ID && e.idPost = p.ID order by e.date desc");

			if ($stripAndChop && $includeBody)
			{
				foreach ($res as $event) 
							{
								$event->body = strip_tags($event->body);
								$event->body = substr($event->body, 0, $chars);
								$event->body .= "...";
							}
			}

			return $res;
		}

		public function getEventListOffset($idSupervisor, $page, $limit, $includeBody = false, $stripAndChop = false, $chars = 400)
		{
			$l = (int) $limit; if ($l < 0) $l = 0;
			$s = (int) $page; $s *= $l; if ($s < 0) $s = 0;
			

			$res;
			if ($includeBody)
			{
				if ($idSupervisor != NULL)
					$res = $this->_DB->query("select e.ID, p.title, p.subtitle, p.body, CONCAT(s.name, ' ', s.surname) as supervisor, e.date, e.idSupervisor from supervisor s join event e join post p on e.idSupervisor = s.ID && e.idPost = p.ID where e.idSupervisor = ? order by e.date desc limit ".$s.", ".$l, Array($idSupervisor));
				else
					$res = $this->_DB->query("select e.ID, p.title, p.subtitle, p.body, CONCAT(s.name, ' ', s.surname) as supervisor, e.date, e.idSupervisor from supervisor s join event e join post p on e.idSupervisor = s.ID && e.idPost = p.ID order by e.date desc limit ".$s.", ".$l);
			}
			else
			{
				if ($idSupervisor != NULL)
					$res = $this->_DB->query("select e.ID, p.title, p.subtitle, CONCAT(s.name, ' ', s.surname) as supervisor, e.date, e.idSupervisor from supervisor s join event e join post p on e.idSupervisor = s.ID && e.idPost = p.ID where e.idSupervisor = ? order by e.date desc limit ".$s.", ".$l, Array($idSupervisor));
				else
					$res = $this->_DB->query("select e.ID, p.title, p.subtitle, CONCAT(s.name, ' ', s.surname) as supervisor, e.date, e.idSupervisor from supervisor s join event e join post p on e.idSupervisor = s.ID && e.idPost = p.ID order by e.date desc limit ".$s.", ".$l);
			}

			if ($stripAndChop && $includeBody)
			{
				foreach ($res as $event) 
							{
								$event->body = strip_tags($event->body);
								$event->body = substr($event->body, 0, $chars);
								$event->body .= "...";
							}
			}

			return $res;
		}	

		public function getCount()
		{
			$res = $this->_DB->query("select count(*) as count from event");
			return $res === false ? 0 : $res[0]->count;
		}

		public function remove($id)
		{
			$this->_MAIL->sendEventRemove($id);
			$this->_DB->query("delete e, p from event e join post p on e.idPost = p.ID where e.ID = ?", Array($id));
		}

		public function getEvent($id)
		{
			$res = $this->_DB->query("select e.ID, e.idSupervisor, e.date, p.title, p.subtitle, p.body, CONCAT(s.name, ' ', s.surname) as supervisor from supervisor s join event e join post p on e.idSupervisor = s.ID && e.idPost = p.ID where e.ID = ?", Array($id));

			return count($res) > 0 ? $res[0] : false;
		}

		public function update($id, $title, $subtitle, $date, $idSupervisor, $body)
		{
			$res = $this->_DB->query("update event e join post p on e.idPost = p.ID set p.title = ?, p.subtitle = ?, e.date = ?, e.idSupervisor = ?, p.body = ? where e.ID = ?", Array($title, $subtitle, $date, $idSupervisor, $body, $id));

			if ($res === false)
				return false;
			else 
			{
				$this->_MAIL->sendEventUpdate($id);
				return true;
			}
		}

		public function getUpcomingEvents($stripAndChop = false, $chars = 400)
		{
			$res = $this->_DB->query("select e.ID, p.title, p.subtitle, p.body, CONCAT(s.name, ' ', s.surname) as supervisor, e.idSupervisor, e.date from supervisor s join event e join post p on e.idSupervisor = s.ID && e.idPost = p.ID where e.date > NOW() order by e.date asc");

			if ($stripAndChop)
			{
				foreach ($res as $event) 
							{
								$event->body = strip_tags($event->body);
								$event->body = substr($event->body, 0, $chars);
								$event->body .= "...";
							}
			}

			return $res;
		}

	} $EVENT = new Event($DB, $SUPERVISOR, $POST, $MAIL);
?>