<?php

	require_once($ROOTPATH."php/database_class.php");
	
	class User
	{
		private $_DB;

		function __construct($DB)
		{
			$this->_DB = $DB;
		}

		public function add($name, $surname, $email)
		{
			$name = strtolower($name);
			$surname = strtolower($surname);
			$email = strtolower($email);

			if ($this->_DB->query("insert into user(name, surname, email) values(?, ?, ?) on duplicate key update name = ?, surname = ?, email = ?", Array($name, $surname, $email, $name, $surname, $email)) === false)
				return false;
			return true;
		}

		public function exists($email)
		{
			$email = strtolower($email);

			$res = $this->_DB->query("select ID from user where email = ?", Array($email));
			return count($res) == 0 || $res === false ? false : $res[0]->ID;
		}

		public function addReturnId($name, $surname, $email)
		{
			$name = strtolower($name);
			$surname = strtolower($surname);
			$email = strtolower($email);

			$res = $this->_DB->query("insert into user(name, surname, email) values (?, ?, ?)", Array($name, $surname, $email));
			return $res ===  false ? false : $this->_DB->lastInsertId();
		}

	} $USER = new User($DB);

?>