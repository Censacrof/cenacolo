<?php

	require_once($ROOTPATH."php/database_class.php");
	require_once($ROOTPATH."php/post_class.php");

	class Supervisor
	{
		private $_DB;
		private $_POST;

		function __construct($DB, $POST)
		{
			$this->_DB = $DB;
			$this->_POST = $POST;
		}

		public function add($name, $surname, $email, $body)
		{
			$title = $name." ".$surname;

			$this->_DB->_pdo->beginTransaction();

			if (!$this->_POST->add($title, "Note sul relatore", $body, 3))
			{
				$this->_DB->_pdo->rollBack();
				return false;
			}

			$res = $this->_DB->query("insert into supervisor(name, surname, email, idPost) values(?, ?, ?, ?)", Array($name, $surname, $email, $this->_DB->lastInsertId()));

			$last = $this->_DB->lastInsertId();

			if ($res === false)
			{
				$this->_DB->_pdo->rollBack();
				return false;
			} else
			{
				$this->_DB->_pdo->commit();
				return $last;
			}
		}

		public function getSupervisorList()
		{
			return $this->_DB->query("select * from supervisor");
		}

		public function getSupervisorListOffset($page, $limit, $includeBody = false, $stripAndChop = false, $chars = 400)
		{
			$l = (int) $limit; if ($l < 0) $l = 0;
			$s = (int) $page; $s *= $l; if ($s < 0) $s = 0;
			

			$res;
			if ($includeBody)
				$res = $this->_DB->query("select s.*, p.body, (select count(*) from event ev join supervisor su on ev.idSupervisor = su.ID where ev.idSupervisor = s.ID) as nEvents from supervisor s join post p on s.idPost = p.ID order by s.name, s.surname asc limit ".$s.", ".$l);
			else
				$res = $this->_DB->query("select s.*, (select count(*) from event ev join supervisor su on ev.idSupervisor = su.ID where ev.idSupervisor = s.ID) as nEvents from supervisor s order by s.name, s.surname asc limit ".$s.", ".$l);

			if ($stripAndChop && $includeBody)
			{
				foreach ($res as $supervisor) 
							{
								$supervisor->body = strip_tags($supervisor->body);
								$supervisor->body = substr($supervisor->body, 0, $chars);
								$supervisor->body .= "...";
							}
			}

			return $res;
		}

		public function remove($id)
		{
			$res = $this->_DB->query("delete s, p from supervisor s join post p on s.idPost = p.ID where s.ID = ?", Array($id));

			return $res === false ? false : true;
		}

		public function getSupervisor($id)
		{
			$res = $this->_DB->query("select s.*, p.body, (select count(*) from event ev join supervisor su on ev.idSupervisor = su.ID where ev.idSupervisor = s.ID) as nEvents from supervisor s join post p on s.idPost = p.ID where s.ID = ?", Array($id));

			if (count($res) > 0)
				return $res[0];
			else return false;
		}

		public function getCount()
		{
			$res = $this->_DB->query("select count(*) as count from supervisor");

			return $res === false ? 0 : $res[0]->count;
		}

		public function update($id, $name, $surname, $email, $body)
		{
			if ($id == NULL || $name == NULL || $surname == NULL || $email == NULL || $body == NULL)
				return false;

			$res = $this->_DB->query("update supervisor s join post p on s.idPost = p.ID set s.name = ?, s.surname = ?, s.email = ?, p.body = ? where s.ID = ?", Array($name, $surname, $email, $body, $id));

			return $res === false ? false : true;
		}

	} $SUPERVISOR = new Supervisor($DB, $POST);

?>