<?php
	$COPYRIGHT = "Cenacolo Andrea da Grosseto &copy 2016";
	$TITLE = "Cenacolo - ";

	//rootpath
	$ROOTPATH = "";

	//funzioni globali
	function randomString($length)
	{   
	    $charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	    $key = "";
	    for($i=0; $i<$length; $i++) 
	        $key .= $charset[(mt_rand(0,(strlen($charset)-1)))]; 

	    return $key;
	}

	function sqlToDateTime($date)
	{
		$d = new DateTime($date);
		return $d->format("d/m/Y H:i");
	}

	function sqlToSortTable($date)
	{
		$d = new DateTime($date);
		return $d->format("YmdHis");
	}

	
	function sqlToSpanDate($date)
	{
		$arr = Array('01' => 'Gen<span>io</span>', '02' => 'Feb<span>braio</span>', '03' => 'Mar<span>zo</span>', '04' => 'Apr<span>ile</span>', '05' => 'Mag<span>gio</span>', '06' => 'Giu<span>gno</span>', '07' => 'Lug<span>lio</span>', '08' => 'Ago<span>sto</span>', '09' => 'Set<span>tembre</span>', '10' => 'Ott<span>obre</span>', '11' => 'Nov<span>embre</span>', '12' => 'Dic<span>embre</span>');

		$datetime = new DateTime($date);
		$d = '<span class="day">'.$datetime->format('d').'</span>';
		$y = '<span class="year">, '.$datetime->format('Y').'</span>';
		$m = '<span class="month">'.$arr[$datetime->format('m')].'</span>';
		$h = '<span>'.$datetime->format('H:i').'</span>';

		return '<span class="date">'.$m.' '.$d.$y.' '.$h.'</span>';
	}

	function echoPagination($page, $tot, $limit, $pageno)
	{
		$totPages = ceil($tot / $limit);

		echo '<div class="pagination"><div class="pages">';								
					if ($pageno > 1) echo '<a href="index.php?page='.$page.'&pageno=0">1</a><span>&hellip;</span>';
					if ($pageno > 0) echo '<a href="index.php?page='.$page.'&pageno='.($pageno - 1).'" >'.$pageno.'</a>';
		echo		'<a class="active">'.($pageno + 1).'</a>';
					if ($pageno < $totPages - 1) echo '<a href="index.php?page='.$page.'&pageno='.($pageno + 1).'">'.($pageno + 2).'</a>';
					if ($pageno < $totPages - 2) echo '<span>&hellip;</span><a href="index.php?page='.$page.'&pageno='.($totPages - 1).'">'.$totPages.'</a>';	
		echo '</div></div>';
	}

	function testCaptcha()
	{
		$postfields = array('secret' => '6LemeAwUAAAAAOwBK3ch9Pz6Qo7u-TZ51HMgYOeX', 'response' => $_POST['g-recaptcha-response'], 'remoteip' => $_SERVER['REMOTE_ADDR']);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
		$result = curl_exec($ch);

		$obj = json_decode($result);

		if (!isset($obj->{'success'}))
			return false;

		if ($obj->{'success'} == true)
			return true;
		else
			return false;
	}
?>