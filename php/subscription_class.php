<?php
	
	require_once($ROOTPATH."php/database_class.php");
	require_once($ROOTPATH."php/user_class.php");
	require_once($ROOTPATH."php/event_class.php");
	require_once($ROOTPATH."php/mail_class.php");
	
	class Subscription
	{
		private $_DB;
		private $_USER;
		private $_EVENT;
		private $_MAIL;

		function __construct($DB, $USER, $EVENT, $MAIL)
		{
			$this->_DB = $DB;
			$this->_USER = $USER;
			$this->_EVENT = $EVENT;
			$this->_MAIL = $MAIL;
		}

		public function subscribe($name, $surname, $email, $sits, $meal, $idEvent)
		{
			$name = strtolower($name);
			$surname = strtolower($surname);
			$email = strtolower($email);

			$meal = (int) $meal;
			$meal = $meal == 0 || $meal == 1 ? $meal : 0;
			//$meal = (bool) $meal;

			$ev = $this->_EVENT->getEvent($idEvent);
			if ($ev === false) return false;
			if (new DateTime() > new DateTime($ev->date))
				return false;

			$idUser = $this->_USER->exists($email);
			if ($idUser === false)
			{
				$idUser = $this->_USER->addReturnId($name, $surname, $email);
				if ($idUser === false)
					return false;
			}

			$t_confirm = md5($this->randomString(32));

			$res = $this->_DB->query("insert into subscription(sits, meal, idEvent, idUser, t_confirm) values(?, ?, ?, ?, ?) on duplicate key update sits = ?, meal = ?, t_confirm = ?, isConfirmed = false", Array($sits, $meal, $idEvent, $idUser, $t_confirm, $sits, $meal, $t_confirm));

			if ($res === false)
				return false;

			$this->_MAIL->sendSubscriptionConfirmation($email, $t_confirm);

			return true;
		}

		public function randomString($length)
		{   
		    $charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

		    $key = "";
		    for($i=0; $i<$length; $i++) 
		        $key .= $charset[(mt_rand(0,(strlen($charset)-1)))]; 

		    return $key;
		}

		public function getParticipants($idEvent)
		{
			$res = $this->_DB->query("select sum(s.sits) as partecipants from subscription s join event e on s.idEvent = e.ID where s.isConfirmed = 1 and e.ID = ?", Array($idEvent));

			if (count($res) == 0)
				return 0;

			return $res[0]->partecipants;
		}

		public function confirm($t_confirm)
		{
			$res = $this->_DB->query("select ID from subscription where t_confirm = ?", Array($t_confirm));
			if (count($res) == 0)
				return false;

			$this->_DB->query("update subscription set isConfirmed = 1 where t_confirm = ?", Array($t_confirm));
			return true;
		}

		public function getSubscribers($idEvent)
		{
			return $this->_DB->query("select s.ID, u.name, u.surname, u.email, s.sits, s.meal from subscription s join user u on s.idUser = u.ID where s.isConfirmed = 1 && s.idEvent = ? order by u.name, u.surname", Array($idEvent));
		}

	} $SUBSCRIPTION = new Subscription($DB, $USER, $EVENT, $MAIL);
?>