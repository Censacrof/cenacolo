<?php

	require_once($ROOTPATH."php/database_class.php");

	class Page
	{
		private $_DB;

		function __construct($DB)
		{
			$this->_DB = $DB;
		}

		public function getPageList($nPosts = false)
		{
			if ($nPosts)
				return $this->_DB->query("select pa.*, (select count(*) from post po where po.idPage = pa.ID) as nPosts from page pa");
			else
				return $this->_DB->query("select * from page");
		}

		public function getSimplePostPageList()
		{
			return $this->_DB->query("select * from page where simplePost = true");
		}

		public function pageExists($page)
		{
			$res = $this->_DB->query("select ID from page where name = ? limit 1", array($page));

			if (count($res) > 0) 
				return true;
			else return false;
		}

		public function isSimplePost($ID)
		{
			$res = $this->_DB->query("select ID from page where ID = ? && simplePost = true limit 1", array($ID));
			if (count($res) > 0)
				return true;
			else return false;
		}

		public function pageExistsID($ID)
		{
			$res = $this->_DB->query("select ID from page where ID = ? limit 1", array($ID));

			if (count($res) > 0) 
				return true;
			else return false;
		}

		public function getPosts($page)
		{
			return $this->_DB->query("select po.* from page pa join post po on po.idPage = pa.ID where pa.name = ? order by date desc", array($page));
		}

		public function getPostsOffset($page, $pageno, $limit)
		{
			$l = (int) $limit; if ($l < 0) $l = 0;
			$s = (int) $pageno; $s *= $l; if ($s < 0) $s = 0;
			

			$res = $this->_DB->query("select po.* from page pa join post po on po.idPage = pa.ID where pa.name = ? order by date desc limit ".$s.", ".$l, Array($page));

			return $res;
		}

		public function getPostCount($page)
		{
			$res = $this->_DB->query("select count(*) as count from page pa join post po on po.idPage = pa.ID where pa.name = ?", Array($page));

			return $res === false ? 0 : $res[0]->count;
		}

		public function getNameByID($ID)
		{
			$res = $this->_DB->query("select name from page where ID = ? limit 1", Array($ID));

			if (count($res) > 0) 
				return $res[0]->name;

			return "";
		}

		public function add($name)
		{
			$res = $this->_DB->query("insert into page(name) values(?)", Array($name));
			return $res === false ? false : true;
		}

		public function emptyPage($id)
		{
			$this->_DB->query("delete from post where idPage = ?", array($id));
		}

		public function remove($id)
		{
			$this->_DB->query("delete from page where ID = ?", array($id));
		}

	} $PAGE = new Page($DB);

?>