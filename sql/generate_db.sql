drop database my_cenacoloandreadagrosseto;
create database my_cenacoloandreadagrosseto character set utf8;
use my_cenacoloandreadagrosseto;

create table page 
	(
		ID int not null auto_increment,
		name varchar(20) not null,
		permanent bit not null default false,
		simplePost bit not null default true,
		primary key(ID),
		unique(ID),
		unique(name)
	);


create table post
	(
		ID int not null auto_increment,
		title varchar(50) null,
		subtitle varchar(200) null,
		body text not null,
		idPage int not null,
		date datetime not null default CURRENT_TIMESTAMP,
		primary key(ID),
		unique(ID),
		foreign key(idPage) references page(ID)
			on delete cascade
			on update cascade
	);

create table user
	(
		ID int not null auto_increment,
		name varchar(20) not null,
		surname varchar(20) not null,
		email varchar(255) not null,
		primary key(ID),
		unique(ID),
		unique(email)
	);

create table admin
	(
		ID int not null auto_increment,
		name varchar(20) not null,
		surname varchar(20) not null,
		email varchar(255) not null,
		level int not null default 0,
		password char(64) not null,
		salt char(64) not null,
		s_token char(32) null,
		s_ip varchar(45) null,
		primary key(ID),
		unique(ID),
		unique(email)
	);

create table supervisor
	(
		ID int not null auto_increment,
		name varchar(20) not null,
		surname varchar(20) not null,
		email varchar(255) not null,
		idPost int not null,
		primary key(ID),
		unique(ID),
		unique(email),
		foreign key(idPost) references post(ID)
			on delete cascade
			on update cascade
	);

create table event 
	(
		ID int not null auto_increment,
		date datetime not null,
		idSupervisor int not null,
		idPost int not null,
		primary key(ID),
		unique(ID),
		foreign key(idSupervisor) references supervisor(ID)
			on delete cascade
			on update cascade,
		foreign key(idPost) references post(ID)
			on delete cascade
			on update cascade
	);

create table subscription
	(
		ID int not null auto_increment,
		sits int not null,
		idEvent int not null,
		idUser int not null,
		t_confirm char(32) not null,
		isConfirmed bit not null default false,
		meal tinyint(1)	not null default false,
		primary key(ID),
		unique(ID),
		unique(idEvent, idUser),
		foreign key(idEvent) references event(ID)
			on update cascade
			on delete cascade,
		foreign key(idUser) references user(ID)
			on update cascade
			on delete cascade
	);

/* aggiungo admin */
insert into admin(name, surname, email, level, password, salt) values ("Cristina", "Torresan", "cristorr03@gmail.com", 1, "fea4c4e3399f2af13cc78d831915238c3d99466f18f38d1721ec345f8210075f", "N0gE6BRBmvllTuiYLB6HlqJD85nJiqii5FcBZ9ZyYX9RwJSe0X1XFOl03iDrCfOZ");

/* aggiungo pagine permanenti NON CAMBIARE L' ORDINE DI INSERIMENTO DELLE PAGINE!!!! */
insert into page(name, permanent, simplePost) values
	("Home", true, true),
	("Eventi", true, false),
	("Relatori", true, false),
	("Chi siamo", true, true),
	("Contattaci", true, true);